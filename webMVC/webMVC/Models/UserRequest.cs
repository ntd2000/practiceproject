﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace webMVC.Models
{
    public class UserRequest
    {
        public Guid Id { get; set; }
        [EmailAddress]
        [Required]
        public string Email { get; set; }
        [Required]
        public string Name { get; set; }

        public string? Password { get; set; }

        public Guid? TenantId { get; set; }

        public Tenant Tenant { get; set; }

        public Guid? RoleId { get; set; }

        public Role Role { get; set; }
    }
}

