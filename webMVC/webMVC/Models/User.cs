﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace webMVC.Models
{
    public class User
    {
        public Guid Id { get; set; }
        [EmailAddress]
        [Required]
        public string Email { get; set; }
        
        [Required]
        public string Password { get; set; }
        [Required]
        public string Name { get; set; }

        public Guid? TenantId { get; set; }

        public Guid? RoleId { get; set; }
    }
}
