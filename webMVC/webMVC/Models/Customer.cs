﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace webMVC.Models
{
    public enum GenderType
    {
        Male,
        Female,
        Others
    }

    public class Customer
    {
        public Guid Id { get; set; }
        public Guid? TenantsId { get; set; }
        public Guid? FormTypeId { get; set; }

        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [EmailAddress]
        [Required]
        public string Email { get; set; }
        [Phone]
        [Required]
        public string Phone { get; set; }
        [Phone]
        [Required]
        public string MobilePhone { get; set; }
        [Required]
        public GenderType Gender { get; set; }
        public string Properties { get; set; }

     
        
    }
}
