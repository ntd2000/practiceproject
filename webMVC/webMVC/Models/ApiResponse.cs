﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webMVC.Models
{
    public class ApiResponse
    {
        public Guid Id { get; set; }
        public string token { get; set; }

        public string RefreshToken { get; set; }
    }


}
