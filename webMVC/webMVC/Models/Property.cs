﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webMVC.Models
{
    public class Property
    {
        public string code { get; set; }
        public string detail { get; set; }
    }
}
