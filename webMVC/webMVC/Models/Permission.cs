﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace webMVC.Models
{
    public class Permission
    {
      
        public Guid Id { get; set; }
        [Required]
        public string PermissionType { get; set; }
      
    }
}
