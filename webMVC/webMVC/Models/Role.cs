﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace webMVC.Models
{
    public class Role
    {
        public Guid Id { get; set; }
        [Required]
        public string NameRole { get; set; }

        public List<Guid>? PermissionId { get; set; }
    }
}
