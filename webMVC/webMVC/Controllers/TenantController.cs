﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using webMVC.Models;

namespace webMVC.Controllers
{
    public class TenantController : Controller
    {
        Uri baseAddress = new Uri("https://localhost:5001/api");
        HttpClient client;
        public TenantController()
        {          
            client = new HttpClient();        
            client.BaseAddress = baseAddress;
          
        }

        private async Task<bool> RenewToken(HttpResponseMessage response)
        {
            HttpHeaders headers = response.Headers;
            string values = headers.ToString();
            if (values.Contains("Token-Expired: true") && response.StatusCode.ToString().Contains("Unauthorize"))
            {
                TokenModel tokenModel = new TokenModel { AccessToken = HttpContext.Session.GetString("JwToken"), RefreshToken = HttpContext.Session.GetString("RefreshToken") };
                string data = JsonConvert.SerializeObject(tokenModel);
                StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                response = client.PostAsync(client.BaseAddress + "/Users/RenewToken", content).Result;
                if (response.IsSuccessStatusCode)
                {
                    string newTemp = await response.Content.ReadAsStringAsync();
                    ApiResponse newModel = JsonConvert.DeserializeObject<ApiResponse>(newTemp);
                    HttpContext.Session.Clear();
                    HttpContext.Session.SetString("JwToken", newModel.token);
                    HttpContext.Session.SetString("RefreshToken", newModel.RefreshToken);
                    HttpContext.Session.SetString("IdUser", newModel.Id.ToString());
                }
                return true;

            }
            return false;


        }
        public async Task<IActionResult> Index()
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            List<Tenant> modelList = new List<Tenant>();
            HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/Tenants").Result;
            if(response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                modelList=JsonConvert.DeserializeObject<List<Tenant>>(data);
                return View("Views/Tenant/Index.cshtml", modelList);
            }
            if(await RenewToken(response)==true)
            {
                return RedirectToAction("Index");
            }
          
            return Redirect("~/Home/NotFound");
        }

        public async Task<ActionResult> Edit(Guid id)
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            Tenant model = new Tenant();
            HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/Tenants/"+id).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                model = JsonConvert.DeserializeObject<Tenant>(data);
                return View("Edit", model);
            }
            if(await RenewToken(response)==true)
            {
                return RedirectToAction("Edit");
            }
            return Redirect("~/Home/NotFound");
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Tenant model)
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            string data = JsonConvert.SerializeObject(model);
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PutAsync(client.BaseAddress + "/Tenants/"+model.Id, content).Result;
            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            if (await RenewToken(response) == true)
            {
                return RedirectToAction("Edit");
            }
            return Redirect("~/Home/NotFound");
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Tenant model)
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            string data = JsonConvert.SerializeObject(model);
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(client.BaseAddress + "/Tenants", content).Result;
            if(response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            if (await RenewToken(response) == true)
            {
                return RedirectToAction("Create");
            }
            return Redirect("~/Home/NotFound");
        }

        
        public async Task<IActionResult> Delete(Guid id)
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            HttpResponseMessage response = client.DeleteAsync(client.BaseAddress + "/Tenants/"+id).Result;
            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            if (response.Content.ReadAsStringAsync().GetAwaiter().GetResult().Contains("haveforeignkey"))
            {
                SetAlert("Can't delete this formtype because user or customer have this foreign key", 2);
                return RedirectToAction("Index");
            }
            if (await RenewToken(response) == true)
            {
                return RedirectToAction("Delete");
            }
            return Redirect("~/Home/NotFound");


        }

        public async Task<IActionResult> Details(Guid id)
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            Tenant model = new Tenant();
            HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/Tenants/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                model = JsonConvert.DeserializeObject<Tenant>(data);
                return View("Details", model);
            }
            if (await RenewToken(response) == true)
            {
                return RedirectToAction("Details");
            }
            return Redirect("~/Home/NotFound");
        }

        protected void SetAlert(string message, int type)
        {
            TempData["AlertMessage"] = message;
            if (type == 1)
            {
                TempData["AlertType"] = "alert-success";
            }
            else if (type == 2)
            {
                TempData["AlertType"] = "alert-warning";
            }
            else if (type == 3)
            {
                TempData["AlertType"] = "alert-danger";
            }
            else
            {
                TempData["AlertType"] = "alert-info";
            }
        }
    }
}
