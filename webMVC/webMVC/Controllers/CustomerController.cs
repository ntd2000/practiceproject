﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using webMVC.Models;

namespace webMVC.Controllers
{
    public class CustomerController : Controller
    {
        Uri baseAddress = new Uri("https://localhost:5001/api");
        HttpClient client;
        public CustomerController()
        {
            client = new HttpClient();
            client.BaseAddress = baseAddress;
        }

        private async Task<bool> RenewToken(HttpResponseMessage response)
        {
            HttpHeaders headers = response.Headers;
            string values = headers.ToString();
            if (values.Contains("Token-Expired: true") && response.StatusCode.ToString().Contains("Unauthorize"))
            {
                TokenModel tokenModel = new TokenModel { AccessToken = HttpContext.Session.GetString("JwToken"), RefreshToken = HttpContext.Session.GetString("RefreshToken") };
                string data = JsonConvert.SerializeObject(tokenModel);
                StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                response = client.PostAsync(client.BaseAddress + "/Users/RenewToken", content).Result;
                if (response.IsSuccessStatusCode)
                {
                    string newTemp = await response.Content.ReadAsStringAsync();
                    ApiResponse newModel = JsonConvert.DeserializeObject<ApiResponse>(newTemp);
                    HttpContext.Session.Clear();
                    HttpContext.Session.SetString("JwToken", newModel.token);
                    HttpContext.Session.SetString("RefreshToken", newModel.RefreshToken);
                    HttpContext.Session.SetString("IdUser", newModel.Id.ToString());
                }
                return true;

            }
            return false;


        }

        protected void SetAlert(string message, int type)
        {
            TempData["AlertMessage"] = message;
            if (type == 1)
            {
                TempData["AlertType"] = "alert-success";
            }
            else if (type == 2)
            {
                TempData["AlertType"] = "alert-warning";
            }
            else if (type == 3)
            {
                TempData["AlertType"] = "alert-danger";
            }
            else
            {
                TempData["AlertType"] = "alert-info";
            }
        }

        public async Task<IActionResult> Index()
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            List<Customer> modelList = new List<Customer>();
            HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/Customers").Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                modelList = JsonConvert.DeserializeObject<List<Customer>>(data);

                return View("Views/Customer/Index.cshtml", modelList);
            }
            else
            {
                SetAlert(response.StatusCode.ToString(), 2);
            }
            if(await RenewToken(response)==true)
            {
                return RedirectToAction("Index");
            }
            return Redirect("~/Home/NotFound");
        }

        private List<Tenant> LoadListTenant()
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            List<Tenant> modelList = new List<Tenant>();
            HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/Tenants").Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                modelList = JsonConvert.DeserializeObject<List<Tenant>>(data);
                return modelList;
            }
            return null;
        }

        private List<FormType> LoadListFormType()
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            List<FormType> modelList = new List<FormType>();
            HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/FormTypes").Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                modelList = JsonConvert.DeserializeObject<List<FormType>>(data);
                return modelList;

            }
            return null;
        }

        public async Task<ActionResult> Edit(Guid id)
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            List<Tenant> list = LoadListTenant();
            List<FormType> formType = LoadListFormType();
            List<Property> property = new List<Property>();
            Customer model = new Customer();
            
            HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/Customers/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                model = JsonConvert.DeserializeObject<Customer>(data);
               property= JsonConvert.DeserializeObject<List<Property>>(model.Properties);
                ViewBag.Item = list;
                ViewBag.FormType = formType;
                ViewBag.List = property;

                return View("Edit", model);
            }
            if (await RenewToken(response) == true)
            {
                return RedirectToAction("Edit");
            }
            return Redirect("~/Home/NotFound");
         
            
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Customer model)
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            string data = JsonConvert.SerializeObject(model);
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PutAsync(client.BaseAddress + "/Customers/" + model.Id, content).Result;
            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            if (await RenewToken(response) == true)
            {
                return RedirectToAction("Edit");
            }
            return Redirect("~/Home/NotFound");
        }

        public async Task<ActionResult> Create()
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            List<Tenant> list = LoadListTenant();
            ViewBag.Item = list;
            List<FormType> formType = LoadListFormType();
            ViewBag.FormType = formType;
            if (list == null)
            {
                HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/Tenants").Result;
                if (await RenewToken(response) == true)
                {
                    return RedirectToAction("Create");
                }
            }
            if (formType==null)
            {
                HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/FormTypes").Result;
                if (await RenewToken(response)==true)
                {
                    return RedirectToAction("Create");
                }
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Customer model)
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            string data = JsonConvert.SerializeObject(model);
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(client.BaseAddress + "/Customers", content).Result;
            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            if (await RenewToken(response) == true)
            {
                return RedirectToAction("Create");
            }
            return View("Views/Home/NotFound.cshtml");
        }


        public async Task<IActionResult> Delete(Guid id)
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            HttpResponseMessage response = client.DeleteAsync(client.BaseAddress + "/Customers/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            if (await RenewToken(response) == true)
            {
                return RedirectToAction("Delete");
            }
            return Redirect("~/Home/NotFound");


        }

        private async Task<Tenant> GetTenantById(Guid? id)
        {
            if (id == null)
            {
                return null;
            }
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            Tenant model = new Tenant();
            HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/Tenants/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                model = JsonConvert.DeserializeObject<Tenant>(data);
                return model;
            }
            return null;

        }


        private async Task<FormType> GetFormTypeById(Guid? id)
        {
            if (id == null)
            {
                return null;
            }
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            FormType model = new FormType();
            HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/FormTypes/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                model = JsonConvert.DeserializeObject<FormType>(data);
                return model;
            }
            return null;

        }


        public async Task<IActionResult> Details(Guid id)
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);          
            Customer model = new Customer();
            List<Property> property = new List<Property>();
            HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/Customers/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                model = JsonConvert.DeserializeObject<Customer>(data);
                property = JsonConvert.DeserializeObject<List<Property>>(model.Properties);            
                ViewBag.List = property;
                if (model.TenantsId != null)
                {
                    Tenant tenant = await GetTenantById(model.TenantsId);

                    ViewData["Tenant"] = tenant.Name;

                }
                if (model.FormTypeId != null)
                {
                    FormType formType = await GetFormTypeById(model.FormTypeId);
                    ViewBag.FormType = formType;
                }
                return View("Details", model);
            }
            if (await RenewToken(response) == true)
            {
                return RedirectToAction("Details");
            }
            return Redirect("~/Home/NotFound");
            
        }
/*
        [HttpGet]
        public IActionResult GetFormType()
        {
            var name = HttpContext.Request.Query["term"].ToString();
            List<FormType> formTypes=LoadListFormType();
            var data = formTypes.Where(f => f.Code.Contains(name)).Select(f=>f.Code }).ToList();
            return Ok(data);
        }*/


    }
}
