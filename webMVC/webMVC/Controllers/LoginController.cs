﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using webMVC.Models;

namespace webMVC.Controllers
{
    public class LoginController : Controller
    {
        Uri baseAddress = new Uri("https://localhost:5001/api");
        HttpClient client;
        public LoginController()
        {
            client = new HttpClient();
            client.BaseAddress = baseAddress;
        }
        public IActionResult Index()
        {
            
            return View("Views/Login/Index.cshtml");
        }

       
        public async Task<IActionResult> LoginUser(User user)
        {
            string data = JsonConvert.SerializeObject(user);
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(client.BaseAddress + "/Users/Login", content).Result;
            if(response.IsSuccessStatusCode)
            {
                string temp = await response.Content.ReadAsStringAsync();
                ApiResponse model = JsonConvert.DeserializeObject<ApiResponse>(temp);
                if (model.token == null)
                {
                    ViewBag.Message = "Incorrect Email or Password";
                    return View("Index");
                }
                HttpContext.Session.SetString("JwToken", model.token);
                HttpContext.Session.SetString("RefreshToken", model.RefreshToken);
                HttpContext.Session.SetString("IdUser", model.Id.ToString());

                return Redirect("~/Home/Index");
            }
            ViewBag.Message = "Incorrect Email or Password";
            return View("Index");


        }


        public IActionResult ForgetPasswordIndex()
        {

            return View("ForgetPassword");
        }
        public async Task<IActionResult> ForgetPassword(Mail mail)
        {
            string data = JsonConvert.SerializeObject(mail);
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PutAsync(client.BaseAddress + "/Users/ForgetPassword", content).Result;
            if (response.IsSuccessStatusCode)
            {

                return Redirect("~/Login/Index");
            }
            ViewBag.Message = "Send mail fail";
            return View("ForgetPassword");

            
        }

        public IActionResult LogOut()
        {
            HttpContext.Session.Clear();
            return Redirect("~/Login/Index");
        }
       
        
        public IActionResult Register()
        {
            return View("Register");
        }

        [HttpPost]
        public IActionResult CreateUser(User model)
        {
            string data = JsonConvert.SerializeObject(model);
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(client.BaseAddress + "/Users/Register", content).Result;
            if (response.IsSuccessStatusCode)
            {
                return Redirect("~/Login/Index");
            }
            return View();
        }

      
    }
}
