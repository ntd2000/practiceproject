﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using webMVC.Models;

namespace webMVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        Uri baseAddress = new Uri("https://localhost:44346/");
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public new IActionResult User()
        {
            return Redirect(baseAddress + "User");
        }

        public IActionResult Customer()
        {
            return Redirect(baseAddress + "Customer");
        }


        public IActionResult Role()
        {
            return Redirect(baseAddress + "Role"); ;
        }

        public IActionResult Tenant()
        {
            return Redirect(baseAddress+"Tenant");
        }

        public IActionResult FormType()
        {
            return Redirect(baseAddress + "FormType");
        }

        public IActionResult Permission()
        {
            return Redirect(baseAddress + "Permission");
        }

        public IActionResult Login()
        {
            
            return Redirect(baseAddress + "Login");
        }

        public IActionResult NotFound()
        {
            return View("NotFound");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
