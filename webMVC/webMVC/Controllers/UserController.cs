﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using webMVC.Models;

namespace webMVC.Controllers
{
    public class UserController : Controller
    {
        Uri baseAddress = new Uri("https://localhost:5001/api");
        HttpClient client;
        public UserController()
        {      
            client = new HttpClient();        
            client.BaseAddress = baseAddress;
        }

        private async Task<bool> RenewToken(HttpResponseMessage response)
        {
            HttpHeaders headers = response.Headers;
            string values = headers.ToString();
            if (values.Contains("Token-Expired: true") && response.StatusCode.ToString().Contains("Unauthorize"))
            {
                TokenModel tokenModel = new TokenModel { AccessToken = HttpContext.Session.GetString("JwToken"), RefreshToken = HttpContext.Session.GetString("RefreshToken") };
                string data = JsonConvert.SerializeObject(tokenModel);
                StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                response = client.PostAsync(client.BaseAddress + "/Users/RenewToken", content).Result;
                if (response.IsSuccessStatusCode)
                {
                    string newTemp = await response.Content.ReadAsStringAsync();
                    ApiResponse newModel = JsonConvert.DeserializeObject<ApiResponse>(newTemp);
                    HttpContext.Session.Clear();
                    HttpContext.Session.SetString("JwToken", newModel.token);
                    HttpContext.Session.SetString("RefreshToken", newModel.RefreshToken);
                    HttpContext.Session.SetString("IdUser", newModel.Id.ToString());
                }
                return true;
                
            }
            return false;
            

        }

        private List<Tenant> LoadListTenant()
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            List<Tenant> modelList = new List<Tenant>();
            HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/Tenants").Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                modelList = JsonConvert.DeserializeObject<List<Tenant>>(data);
                return modelList;
            }
         
            return null;
        }

        private async Task<Tenant> GetTenantById(Guid? id)
        {
            if(id==null)
            {
                return null;
            }
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            Tenant model = new Tenant();
            HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/Tenants/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                model = JsonConvert.DeserializeObject<Tenant>(data);
                return model;
            }
            return null;
            
        }


        private async Task<Role> GetRoleById(Guid? id)
        {
            if(id==null)
            {
                return null;
            }
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            Role model = new Role();
            HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/Roles/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                model = JsonConvert.DeserializeObject<Role>(data);
                return model;
            }
            return null;

        }

        private List<Role> LoadListRole()
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            List<Role> modelList = new List<Role>();
            HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/Roles").Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                modelList = JsonConvert.DeserializeObject<List<Role>>(data);
                return modelList;
            }
           
            return null;

        }

        public async Task<IActionResult> Index()
        {

            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            List<User> modelList = new List<User>();
            HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/Users").Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                modelList = JsonConvert.DeserializeObject<List<User>>(data);
                return View("Views/User/Index.cshtml", modelList);
            }

            if(await RenewToken(response)==true)
            {
                return RedirectToAction("Index");
            }
            return Redirect("~/Home/NotFound");
        }

        public async Task<ActionResult> Edit(Guid id)
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            User model = new User();
            HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/Users/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                model = JsonConvert.DeserializeObject<User>(data);
                List<Tenant> tenants =  LoadListTenant();
                List<Role> roles =  LoadListRole();
                ViewBag.Tenant = tenants;
                ViewBag.Role = roles;
                return View("Edit", model);
            }
            if(await RenewToken(response) == true)
            {
                return RedirectToAction("Edit");
            }
            return Redirect("~/Home/NotFound");
            
        }

        [HttpPost]
        public async Task<IActionResult> Edit(UserRequest model)
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            string data = JsonConvert.SerializeObject(model);
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PutAsync(client.BaseAddress + "/Users/" + model.Id, content).Result;
            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            if(await RenewToken(response)==true)
            {
                return RedirectToAction("Edit");
            }
            return Redirect("~/Home/NotFound");
        }

        public async Task<ActionResult> Create()
        {
          
            List<Tenant> tenants = LoadListTenant();

            List<Role> roles = LoadListRole();    
            if(tenants==null)
            {
                HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/Tenants").Result;
                if (await RenewToken(response))
                {
                    return RedirectToAction("Create");
                }
            }
            ViewBag.Tenant = tenants;           
            ViewBag.Role = roles;          
            
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(User model)
        {

            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            string data = JsonConvert.SerializeObject(model);
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(client.BaseAddress + "/Users", content).Result;
            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }           
            if(await RenewToken(response)==true)
            {
                return RedirectToAction("Create");
            }
            return Redirect("~/Home/NotFound");
        }


        public async Task<IActionResult> Delete(Guid id)
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            HttpResponseMessage response = client.DeleteAsync(client.BaseAddress + "/Users/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            if(await RenewToken(response)==true)
            {
                return RedirectToAction("Delete");
            }
            return Redirect("~/Home/NotFound");


        }

        public async Task<IActionResult> Profile()
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            Guid id;
            try
            {
                id = new Guid(HttpContext.Session.GetString("IdUser"));
            }
            catch
            {
                return Redirect("~/Home/NotFound");
            }
           
            User model = new User();
            HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/Users/Profile/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                model = JsonConvert.DeserializeObject<User>(data);
                List<Role> roles = LoadListRole();
                ViewBag.Role = roles;
                return View("Profile", model);
            }
            if (await RenewToken(response) == true)
            {
                return RedirectToAction("Profile");
            }               
            return Redirect("~/Home/NotFound");
           
        }

        [HttpPost]
        public async Task<IActionResult> EditProfile(User model)
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            string data = JsonConvert.SerializeObject(model);
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PutAsync(client.BaseAddress + "/Users/EditProfile/" + model.Id, content).Result;
            if (response.IsSuccessStatusCode)
            {     
                    TokenModel tokenModel = new TokenModel { AccessToken = HttpContext.Session.GetString("JwToken"), RefreshToken = HttpContext.Session.GetString("RefreshToken") };
                    string newData = JsonConvert.SerializeObject(tokenModel);
                    StringContent newContent = new StringContent(newData, Encoding.UTF8, "application/json");
                    response = client.PostAsync(client.BaseAddress + "/Users/RenewToken", newContent).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string newTemp = await response.Content.ReadAsStringAsync();
                        ApiResponse newModel = JsonConvert.DeserializeObject<ApiResponse>(newTemp);
                        HttpContext.Session.Clear();
                        HttpContext.Session.SetString("JwToken", newModel.token);
                        HttpContext.Session.SetString("RefreshToken", newModel.RefreshToken);
                        HttpContext.Session.SetString("IdUser", newModel.Id.ToString());
                    }
                    return Redirect("~/Home/Index");
                
            }
            if(await RenewToken(response)==true)
            {
                return RedirectToAction("EditProfile");
            }
            return Redirect("~/Home/NotFound");
        }

        public async Task<IActionResult> Details(Guid id)
        {
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            User model = new User();
            HttpResponseMessage response = client.GetAsync(client.BaseAddress + "/Users/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                string tempData = response.Content.ReadAsStringAsync().Result;
                model = JsonConvert.DeserializeObject<User>(tempData);
                if(model.TenantId!=null)
                {
                    Tenant tenant = await GetTenantById(model.TenantId);
                 
                    ViewData["Tenant"] = tenant.Name;
                   
                }
                if(model.RoleId!=null)
                {
                    Role role = await GetRoleById(model.RoleId);
                    ViewData["Role"] = role.NameRole;
                }
                return View("Details", model);
            }
            if(await RenewToken(response)==true)
            {
                return RedirectToAction("Details");
            }
            return Redirect("~/Home/NotFound");

        }



        public IActionResult ChangePassword(Guid id)
        {
            ChangePassword model = new ChangePassword();
            model.Id = id;
            return View("ChangePassword", model);
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePassword model)
        {
            if(model.NewPassword!=model.ReNewPassword)
            {
                ViewBag.Message = "RePassword does not same with new Password";
                return View("ChangePassword");
            }
            var accessToken = HttpContext.Session.GetString("JwToken");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            string data = JsonConvert.SerializeObject(model);
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PutAsync(client.BaseAddress + "/Users/ChangePassword/" + model.Id, content).Result;
            if (response.IsSuccessStatusCode)
            { 
                
                return RedirectToAction("Profile");
            }
            if (await RenewToken(response) == true)
            {
                return RedirectToAction("ChangePassword");
            }
            return Redirect("~/Home/NotFound");
        }
    }

}
