﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using demoPractice3.Models;
using demoPractice3.Services;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.Authorization;

namespace demoPractice3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableQuery]
    public class TenantsController : ControllerBase
    {
        private readonly ITenantService _tenantService;

        public TenantsController(ITenantService tenantService)
        {
            _tenantService = tenantService;
        }

        // GET: api/Tenants
       
        [HttpGet]
        [Authorize(Roles = "GetTenants")]
        public async Task<ActionResult<IEnumerable<Tenant>>> GetTenants()
        {
            try
            {
                return await _tenantService.GetAll();
            }
            catch
            {
                return BadRequest();
            }
        }

        // GET: api/Tenants/5
        [HttpGet("{id}")]
        [Authorize(Roles = "GetTenant")]
        public async Task<ActionResult<Tenant>> GetTenant(Guid id)
        {
            var tenant = await _tenantService.GetById(id);

            if (tenant == null)
            {
                return NotFound();
            }

            return tenant;
        }

        // PUT: api/Tenants/5
        [HttpPut("{id}")]
        [Authorize(Roles = "PutTenant")]
        public async Task<IActionResult> PutTenant(Guid id, Tenant tenant)
        {
            if (id != tenant.Id)
            {
                return BadRequest();
            }

            var result = await _tenantService.Update(id, tenant);
            if (result.Equals("null"))
            {
                return NotFound();
            }
            else
            {
                return NoContent();
            }
        }

        // POST: api/Tenants
        [HttpPost]
        [Authorize(Roles = "PostTenant")]
        public async Task<ActionResult<Tenant>> PostTenant(Tenant tenant)
        {
            try
            {
                await _tenantService.Add(tenant);
                return CreatedAtAction("GetTenant", new { id = tenant.Id }, tenant);
            }
            catch
            {
                return BadRequest();
            }
                
        }

        // DELETE: api/Tenants/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "DeleteTenant")]
        public async Task<ActionResult> DeleteTenant(Guid id)
        {
            try
            {
                var tenant = await _tenantService.Delete(id);
                if (tenant.Equals("null"))
                {
                    return NotFound();
                }
                else if (tenant.Equals("foreignkey"))
                {
                    return BadRequest("haveforeignkey");
                }
                else
                {
                    return Ok();
                }
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
