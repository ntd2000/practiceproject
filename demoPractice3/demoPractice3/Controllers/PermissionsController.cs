﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using demoPractice3.Models;
using demoPractice3.Services;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.Authorization;

namespace demoPractice3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    [EnableQuery]
    public class PermissionsController : ControllerBase
    {
        private readonly IPermissionService _permissionService;

        public PermissionsController(IPermissionService permissionService)
        {
            _permissionService = permissionService;
        }

        // GET: api/Permissions
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Permission>>> GetPermissions()
        {
            try
            {

                return await _permissionService.GetAll();
            }
            catch
            {
                return BadRequest();
            }
        }

        // GET: api/Permissions/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Permission>> GetPermission(Guid id)
        {
            var permission =  _permissionService.GetById(id);

            if (permission == null)
            {
                return NotFound();
            }

            return permission;
        }

        [HttpGet("getByRoleId/{id}")]
        [EnableQuery]
        public async Task<ActionResult<IEnumerable<Permission>>> GetPermissionByRoleId(Guid id)
        {
            var permission = await _permissionService.GetByRoleId(id);

            if (permission == null)
            {
                return NotFound();
            }

            return permission;
        }

        // PUT: api/Permissions/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPermission(Guid id, Permission permission)
        {
            if (id != permission.Id)
            {
                return BadRequest();
            }
            var result =await _permissionService.Update(id, permission);
            if (result.Equals("null"))
            {
                return NotFound();
            }
            else
            {
                return NoContent();
            }
        }

        // POST: api/Permissions
        [HttpPost]
        public async Task<ActionResult<Permission>> PostPermission(Permission permission)
        {
            try {
                await _permissionService.Add(permission);

                return CreatedAtAction("GetPermission", new { id = permission.Id }, permission);
            }
            catch
            {
                return BadRequest();
            }
        }

        // DELETE: api/Permissions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeletePermission(Guid id)
        {
            try
            {


                var permission =await  _permissionService.Delete(id);
                if (permission.Equals("null"))
                {
                    return NotFound();
                }
                else
                {
                    return Ok();
                }
            }
            catch
            {
                return BadRequest();
            }
        }

       
    }
}
