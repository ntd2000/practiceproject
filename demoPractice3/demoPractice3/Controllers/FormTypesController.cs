﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using demoPractice3.Models;
using demoPractice3.Services;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.Authorization;

namespace demoPractice3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableQuery]
    public class FormTypesController : ControllerBase
    {
        private readonly IFormTypeService _formTypeService;

        public FormTypesController(IFormTypeService formTypeService)
        {
            _formTypeService = formTypeService;
        }

        // GET: api/FormTypes
        [HttpGet]
        [Authorize(Roles = "GetFormTypes")]
        public async Task<ActionResult<IEnumerable<FormType>>> GetFormTypes()
        {
            try {
                return await _formTypeService.GetAll();
            }
            catch
            {
                return BadRequest();
            }
        }

        // GET: api/FormTypes/5
        [HttpGet("{id}")]
        [Authorize(Roles = "GetFormType")]
        public async Task<ActionResult<FormType>> GetFormType(Guid id)
        {
            var formType = await _formTypeService.GetById(id);

            if (formType == null)
            {
                return NotFound();
            }

            return formType;
        }

        // PUT: api/FormTypes/5
        [HttpPut("{id}")]
        [Authorize(Roles = "PutFormType")]
        public async Task<IActionResult> PutFormType(Guid id, FormType formType)
        {
            if (id != formType.Id)
            {
                return BadRequest();
            }

            var result =  await _formTypeService.Update(id, formType);
            if (result.Equals("null"))
            {
                return NotFound();
            }
            else
            {
                return NoContent();
            }
        }

        // POST: api/FormTypes
        [HttpPost]
        [Authorize(Roles = "PostFormType")]
        public async Task<ActionResult<FormType>> PostFormType(FormType formType)
        {
            try {
                await _formTypeService.Add(formType);

                return CreatedAtAction("GetFormType", new { id = formType.Id }, formType);
            }
            catch
            {
                return BadRequest();
            }
        }


        // DELETE: api/FormTypes/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "DeleteFormType")]
        public async Task<ActionResult> DeleteFormType(Guid id)
        {
            try
            {
                var formType = await _formTypeService.Delete(id);
                if (formType.Equals("null"))
                {
                    return NotFound();
                }
                else if(formType.Equals("foreignkey"))
                {
                    return BadRequest("haveforeignkey");
                }
                else
                {
                    return Ok();
                }

            }
            catch
            {
                return BadRequest();
            }
        }

       
    }
}
