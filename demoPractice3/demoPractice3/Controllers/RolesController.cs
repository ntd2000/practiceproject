﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using demoPractice3.Models;
using demoPractice3.Services;
using Microsoft.AspNetCore.OData.Query;
using demoPractice3.Models.RequestModels;
using Microsoft.AspNetCore.Authorization;

namespace demoPractice3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    [EnableQuery]
    public class RolesController : ControllerBase
    {
        private readonly IRoleService _roleService;

        public RolesController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        // GET: api/Roles
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Role>>> GetRoles()
        {
            try
            {
                return await _roleService.GetAll();
            }
            catch
            {
                return BadRequest();
            }
        }

        // GET: api/Roles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Role>> GetRole(Guid id)
        {
            var role = await _roleService.GetById(id);

            if (role == null)
            {
                return NotFound();
            }

            return role;
        }

        // PUT: api/Roles/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRole(Guid id, RoleRequest role)
        {
            if (id != role.Id)
            {
                return BadRequest();
            }

            var result =await _roleService.Update(id, role);
            if (result.Equals("null"))
            {
                return NotFound();
            }
            else
            {
                return NoContent();
            }
        }

        // POST: api/Roles
        [HttpPost]
        public async Task<ActionResult<Role>> PostRole(RoleRequest role)
        {
            try
            {
               await _roleService.Add(role);
                return CreatedAtAction("GetRole", new { id = role.Id }, role);
            }
            catch
            {
                return BadRequest();
            }
        }

        // DELETE: api/Roles/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteRole(Guid id)
        {
            try
            {


                var role =await _roleService.Delete(id);
                if (role.Equals("null"))
                {
                    return NotFound();
                }
                else if (role.Equals("foreignkey"))
                {
                    return BadRequest("haveforeignkey");
                }
                else
                {
                    return Ok();
                }
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
