﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using demoPractice3.Models;
using demoPractice3.Services;
using Microsoft.AspNetCore.OData.Query;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using System.Security.Cryptography;
using demoPractice3.Models.RequestModels;
using demoPractice3.MailServices;
using demoPractice3.Security;

namespace demoPractice3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableQuery]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IPermissionService _permissionService;

        private readonly AppSettings _appSettings;
        private readonly IMailService _mailService;

        private readonly IRefreshTokenService _refreshTokenService;

        public UsersController(IUserService userService, IOptionsMonitor<AppSettings>optionsMonitor,IPermissionService permissionService,IRefreshTokenService refreshTokenService,IMailService mailService)
        {
            _userService = userService;
            _appSettings = optionsMonitor.CurrentValue;
            _permissionService = permissionService;
            _refreshTokenService = refreshTokenService;
            _mailService = mailService;

        }

        // GET: api/Users
        [HttpGet]
        [Authorize(Roles = "GetUsers")]
        public async Task<ActionResult<IEnumerable<User>>> GetUsers()
        {
            try
            {
                return await _userService.GetAll();
            }
            catch
            {
                return BadRequest();
            }
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        [Authorize(Roles = "GetUser")]
        public async Task<ActionResult<User>> GetUser(Guid id)
        {
            var user = await _userService.GetById(id);

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        [Authorize(Roles = "PutUser")]
        public async Task<IActionResult> PutUser(Guid id, UserRequest data)
        {
            if (id != data.Id)
            {
                return BadRequest();
            }
            User user = await _userService.GetById(data.Id);
            user.Name = data.Name;
            user.RoleId = data.RoleId;
            user.TenantId = data.TenantId;
            user.Email = data.Email;
           
            var result = await _userService.Update(id, user);
            if (result.Equals("null"))
            {
                return NotFound();
            }
            else
            {
                return NoContent();
            }
        }

        // POST: api/Users
        [HttpPost]
        [Authorize(Roles = "PostUser")]
        public async Task<ActionResult<User>> PostUser(User user)
        {
            try
            {
                user.Password = SecurityHelper.Encrypt(user.Password);
                await _userService.Add(user);
                return CreatedAtAction("GetUser", new { id = user.Id }, user);
            }
            catch
            {
                return BadRequest();
            }
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "DeleteUser")]
        public async Task<ActionResult> DeleteUser(Guid id)
        {
            try
            {
                var user =await _userService.Delete(id);
                if (user.Equals("null"))
                {
                    return NotFound();
                }
                else
                {
                    return Ok();
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Login(Login login)
        {
            login.Password = SecurityHelper.Encrypt(login.Password);
            var user =await _userService.GetUserByLogin(login);
          
            if (user==null)
            {
                return BadRequest("User is not exist");
            }
            List<Guid> idList = _userService.GetPermissionByUser(user);
            List<Permission> list = new List<Permission>();
            foreach(Guid item in idList)
            {
                Permission temp = _permissionService.GetById(item);
                list.Add(temp);
            }
            TokenModel model = GenerateToken(user, list);
            return Ok(new ApiResponse { Id=user.Id, Token=model.AccessToken,RefreshToken=model.RefreshToken});
        }


        private TokenModel GenerateToken(User user,List<Permission> list)
        {
           
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.UTF8.GetBytes(_appSettings.SecretKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject =getClaimsIdentity(user,list),
                Expires =DateTime.UtcNow.AddMinutes(2),
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var accessToken= tokenHandler.WriteToken(token);
            var refreshToken = GenderRefreshToken();
            var refreshTokenEntity = new RefreshToken
            {
                Id = Guid.NewGuid(),
                JwtId = token.Id,
                Token = refreshToken,
                UserId=user.Id,
                IsUsed = false,
                IsReVoked = false,
                IssuedAt = DateTime.UtcNow,
                ExpiredAt = DateTime.UtcNow.AddHours(1)
            };

            _refreshTokenService.Add(refreshTokenEntity);
            return new TokenModel
            {
                AccessToken=accessToken,
                RefreshToken=refreshToken
            };
        }

        private string GenderRefreshToken()
        {
            var random = new byte[32];
            using(var rng=RandomNumberGenerator.Create())
            {
                rng.GetBytes(random);
                return Convert.ToBase64String(random);
            }
        }

        private ClaimsIdentity getClaimsIdentity(User user,List<Permission> list)
        {
           
            return new ClaimsIdentity(
                 getClaims()
                );

            Claim[] getClaims()
            {
                List<Claim> claims = new List<Claim>();
                claims.Add(new Claim(JwtRegisteredClaimNames.Email, user.Email));
                claims.Add(new Claim(JwtRegisteredClaimNames.Sub, user.Email));
                claims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
                claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
                claims.Add(new Claim(ClaimTypes.Name, user.Name));
               
                foreach (var item in list)
                {

                    claims.Add(new Claim(ClaimTypes.Role, item.PermissionType));
                }
                return claims.ToArray();
            }

        }

      

        private DateTime ConverUnixTimeToDateTime(long utcExpireDate)
        {
            var dateTimeInterval = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dateTimeInterval.AddSeconds(utcExpireDate).ToUniversalTime();
            return dateTimeInterval;
        }

        [HttpPost("Register")]
        public async Task<ActionResult<User>> CreateUser(User user)
        {
            var regis = new Mail
            {
                Email=user.Email
            };
            User userExists = await _userService.GetUserByMail(regis);
            if(userExists!=null)
            {
                return Ok("user already exists");
            }
            try
            {
                user.Password = SecurityHelper.Encrypt(user.Password);
                await _userService.Add(user);
                return CreatedAtAction("GetUser", new { id = user.Id }, user);
            }
            catch
            {
                return BadRequest("something when wrong");
            }
        }


        [HttpGet("Profile/{id}")]
        [Authorize]
        public async Task<ActionResult<User>> GetUserProfile(Guid id)
        {
            var user = await _userService.GetById(id);

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        // PUT: api/Users/5
        [HttpPut("EditProfile/{id}")]
        [Authorize]
        public async Task<IActionResult> PutUserProfile(Guid id, UserRequest data)
        {
            if (id != data.Id)
            {
                return BadRequest();
            }

            User user = await _userService.GetById(data.Id);
            user.Name = data.Name;
            user.RoleId = data.RoleId;
            user.TenantId = data.TenantId;
            user.Email = data.Email;

            var result = await _userService.Update(id, user);
            if (result.Equals("null"))
            {
                return NotFound();
            }
            else
            {
                return NoContent();
            }
        }

        [HttpPost("RenewToken")]
        public async Task<IActionResult> RenewToken(TokenModel model)
        {
            var jwtTokenHandler = new JwtSecurityTokenHandler();
            var secretKeyBytes = Encoding.UTF8.GetBytes(_appSettings.SecretKey);
            var tokenValidateParam = new TokenValidationParameters
            {
                //tự cấp token
                ValidateIssuer = false,
                ValidateAudience = false,

                //ký vào token
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(secretKeyBytes),

                ClockSkew = TimeSpan.Zero,
                ValidateLifetime = false
            };
            try
            {
                var tokenInVerfication = jwtTokenHandler.ValidateToken(model.AccessToken, tokenValidateParam, out var validatedToken);
                if (validatedToken is JwtSecurityToken jwtSercurityToken)
                {
                    var result = jwtSercurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase);
                    if (!result)
                    {
                        return Ok("Invalid Token");
                    }
                }
                    var utcExpireDate = long.Parse(tokenInVerfication.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Exp).Value);

                    var expireDate = ConverUnixTimeToDateTime(utcExpireDate);

                    if (expireDate > DateTime.UtcNow)
                    {
                        return Ok("Access token has not yet expired");
                    }
                    //lay refresh token
                    var storedToken = _refreshTokenService.Compare(model);
                    if (storedToken == null)
                    {
                        return Ok("Refresh token does not exist");
                    }
                    if (storedToken.IsUsed)
                    {
                        return Ok("Refresh token has been used");
                    }
                    if (storedToken.IsReVoked)
                    {
                        return Ok("Refresh token has been revoked");
                    }
                    var jti = tokenInVerfication.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
                    if (storedToken.JwtId != jti)
                    {
                        return Ok("Token does not match");
                    }
                
                
                storedToken.IsReVoked = true;
                storedToken.IsUsed = true;
                _refreshTokenService.Edit(storedToken);
                User user = await _userService.GetById(storedToken.UserId);
                List<Guid> idList = _userService.GetPermissionByUser(user);
                List<Permission> list = new List<Permission>();
                foreach (Guid item in idList)
                {
                    Permission temp = _permissionService.GetById(item);
                    list.Add(temp);
                }
                TokenModel modelToken = GenerateToken(user, list);
                return Ok(new ApiResponse { Id = user.Id, Token = modelToken.AccessToken, RefreshToken = modelToken.RefreshToken });
            }
            catch (Exception ex)
            {
                return BadRequest("something when wrong");
            }
        }


        [HttpPut("ChangePassword/{id}")]
        [Authorize(Roles = "ChangePassword")]
        public async Task<IActionResult> ChangePassword(Guid id, ChangPassword data)
        {
            if (id != data.Id)
            {
                return BadRequest();
            }
          
            var result = await _userService.ChangePassword(id, data);
            if (result.Equals("null"))
            {
                return NotFound();
            }
            else if(result.Equals("Not correct old password"))
            {
                return Ok("Not correct old password");
            }
            else
            {
                return NoContent();
            }
        }

        [HttpPut("ForgetPassword")]
        public async Task<IActionResult> ForgetPassword(Mail mail)
        {
            User model = await _userService.GetUserByMail(mail);
            if (model == null)
            {
                return BadRequest("User is not exist");
            }

            

            var data = await _mailService.SendEmailAsync(mail.Email, "Gui mat khau", SecurityHelper.Decrypt(model.Password));
            if(data.Equals("send mail fail"))
            {
                return BadRequest("send mail fail");
            }
            return Ok("send mail success");
               
        }

    }
}

