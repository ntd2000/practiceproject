﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using demoPractice3.Models;
using demoPractice3.Services;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.Authorization;

namespace demoPractice3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableQuery]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerService _customerService;

        public CustomersController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        // GET: api/Customers
        [HttpGet]
        [Authorize(Roles = "GetCustomers")]
        public async Task<ActionResult<IEnumerable<Customer>>> GetCustomers()
        {
            try
            {
                return await _customerService.GetAll();
            }
            catch
            {
                return BadRequest();
            }
           
        }

        // GET: api/Customers/5
        [HttpGet("{id}")]
        [Authorize(Roles = "GetCustomer")]
        public async Task<ActionResult<Customer>> GetCustomer(Guid id)
        {
            var customer = await _customerService.GetById(id);

            if (customer == null)
            {
                return NotFound();
            }

            return customer;
        }

        // PUT: api/Customers/5
        [HttpPut("{id}")]
        [Authorize(Roles = "PutCustomer")]
        public async Task<IActionResult> PutCustomer(Guid id, Customer customer)
        {
            if (id != customer.Id)
            {
                return BadRequest();
            }
            var result =await _customerService.Update(id, customer);
            if(result.Equals("null"))
            {
                return NotFound();
            }
            else
            {
                return NoContent();
            }
            
        }

        // POST: api/Customers
        [HttpPost]
        [Authorize(Roles = "PostCustomer")]
        public async Task<ActionResult<Customer>> PostCustomer(Customer customer)
        {
            try
            {
               await _customerService.Add(customer);
                return CreatedAtAction("GetCustomer", new { id = customer.Id }, customer);

            }
            catch
            {
                return BadRequest();
            }
        }
        // DELETE: api/Customers/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "DeleteCustomer")]
        public async Task<ActionResult> DeleteCustomer(Guid id)
        {
            try
            {
                var customer = await _customerService.Delete(id);
                if (customer.Equals("null"))
                {
                    return NotFound();
                }
                else
                {
                    return Ok();
                }

            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
