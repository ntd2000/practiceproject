﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using demoPractice3.Models;
using demoPractice3.Services;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.Authorization;

namespace demoPractice3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    [EnableQuery]
    public class RolePermissionsController : ControllerBase
    {
        private readonly IRolePermissionService _rolePermissionService;


        public RolePermissionsController(IRolePermissionService rolePermissionService)
        {
            _rolePermissionService = rolePermissionService;
        }

        // GET: api/RolePermissions
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RolePermission>>> GetRolePermission()
        {
            try
            {
                return await _rolePermissionService.GetAll();
            }
            catch
            {
                return BadRequest();
            }
        }

        // GET: api/RolePermissions/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RolePermission>> GetRolePermission(Guid RoleId,Guid PermissionId)
        {
            var rolePermission = await _rolePermissionService.GetById(RoleId,PermissionId);

            if (rolePermission == null)
            {
                return NotFound();
            }

            return rolePermission;
        }

        // PUT: api/RolePermissions/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRolePermission(Guid RoleId,Guid PermissionId, RolePermission rolePermission)
        {
            if (RoleId != rolePermission.RoleId && PermissionId!= rolePermission.PermissionId)
            {
                return BadRequest();
            }

            var result =await _rolePermissionService.Update(RoleId,PermissionId, rolePermission);
            if (result.Equals("null"))
            {
                return NotFound();
            }
            else
            {
                return NoContent();
            }
        }

        // POST: api/RolePermissions
        
        [HttpPost]
        public async Task<ActionResult<RolePermission>> PostRolePermission(RolePermission rolePermission)
        {
            try {
                var data =await _rolePermissionService.Add(rolePermission);
                if (data.Equals("conflict"))
                {
                    return Conflict();
                }
                else
                {
                    return CreatedAtAction("GetRolePermission", new { id = rolePermission.RoleId }, rolePermission);
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        // DELETE: api/RolePermissions/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteRolePermission(Guid RoleId,Guid PermissionId)
        {
            try
            {
                var data =await _rolePermissionService.Delete(RoleId,PermissionId);
                if (data.Equals("null"))
                {
                    return NotFound();
                }
                else
                {
                    return Ok();
                }
            }
            catch
            {
                return BadRequest();
            }
        }

       
    }
}
