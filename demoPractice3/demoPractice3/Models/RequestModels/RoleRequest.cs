﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Models.RequestModels
{
    public class RoleRequest
    {
        public Guid Id { get; set; }
        public string NameRole { get; set; }
        public List<Guid> PermissionId { get; set; }
    }
}
