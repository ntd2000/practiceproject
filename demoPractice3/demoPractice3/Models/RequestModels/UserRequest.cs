﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Models.RequestModels
{
    public class UserRequest
    {
        public Guid Id { get; set; }
        public string Email { get; set; }

        public string Name { get; set; }

        public string? Password { get; set; }
        public Guid? TenantId { get; set; }
      
        public Guid? RoleId { get; set; }

        public Tenant Tenant { get; set; }
        public Role Role { get; set; }
     
    }
}
