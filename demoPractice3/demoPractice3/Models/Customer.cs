﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Models
{
    public enum GenderType
    {
        Male,
        Female,
        Others
    }

    [Table("Customer")]
    public class Customer
    {
        [Key]
        public Guid Id { get; set; }

        public Guid? TenantsId { get; set; }
        [ForeignKey("TenantId")]
        public Tenant Tenant { get; set; }
        public Guid? FormTypeId { get; set; }
        [ForeignKey("FormTypeId")]
        public FormType FormType { get; set; }

        [Required(ErrorMessage = "This field is not empty")]
        [MaxLength(512)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "This field is not empty")]
        [MaxLength(512)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "This field is not empty")]
        [MaxLength(256)]
        public string Email { get; set; }

        [Required(ErrorMessage = "This field is not empty")]
        [MaxLength(256)]
        public string Phone { get; set; }

        [Required(ErrorMessage = "This field is not empty")]
        [MaxLength(256)]
        public string MobilePhone { get; set; }

        [Required]
        public GenderType Gender { get; set; }
       
        public string Properties { get; set; }
    }
}
