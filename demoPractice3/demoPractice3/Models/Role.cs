﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Models
{
    [Table("Role")]
    public class Role
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "This field is not empty")]
        [MaxLength(256)]
        public string NameRole { get; set; }


        public ICollection<RolePermission> RolePermissions { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
