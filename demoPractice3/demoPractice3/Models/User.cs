﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Models
{
    [Table("User")]
    public class User
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "This field is not empty")]
        [Column(TypeName = "nvarchar(256)")]
        public string Email { get; set; }

        [Required(ErrorMessage = "This field is not empty")]
        [Column(TypeName = "nvarchar(256)")]
        public string Password { get; set; }

        [Required(ErrorMessage = "This field is not empty")]
        [Column(TypeName = "nvarchar(256)")]
        public string Name { get; set; }

        public Guid? TenantId { get; set; }
        [ForeignKey("TenantId")]
        public Tenant Tenant { get; set; }

        public Guid? RoleId { get; set; }
        [ForeignKey("RoleId")]
        public Role Role { get; set; }


    }
}
