﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Models
{
    [Table("Tenant")]
    public class Tenant
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "This field is not empty")]
        [MaxLength(256)]
        public string Name { get; set; }

        public Tenant()
        {
            Customers = new HashSet<Customer>();
        }
        public ICollection<Customer> Customers { get; set; }
        public virtual ICollection<User> Users { get; set; }


    }
}
