﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Models
{
  

    [Table("Permission")]
    public class Permission
    {
        [Key]
        public Guid Id { get; set; }


        [Required(ErrorMessage = "This field is not empty")]
        public string PermissionType { get; set; }

       

        public ICollection<RolePermission> RolePermissions { get; set; }
      
    }
}
