﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using demoPractice3.Models;

namespace demoPractice3.Models
{
    public class MyDBContext :DbContext
    {
        public MyDBContext(DbContextOptions options ): base(options) { }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<FormType> FormTypes { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Tenant> Tenants { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Customer>()
                .Property(u => u.Gender)
                .HasConversion<string>()
                .HasMaxLength(50);

            modelBuilder.Entity<User>()
                .HasIndex(u => u.Email)
                .IsUnique();

            modelBuilder.Entity<RolePermission>()
               .HasKey(x => new { x.RoleId, x.PermissionId });

            modelBuilder.Entity<RolePermission>()
                .HasOne(x => x.Role)
                .WithMany(y => y.RolePermissions)
                .HasForeignKey(x=>x.RoleId);

            modelBuilder.Entity<RolePermission>()
                .HasOne(x => x.Permission)
                .WithMany(y => y.RolePermissions)
                .HasForeignKey(x => x.PermissionId);

            modelBuilder.Entity<Tenant>()
                .HasMany(t => t.Customers)
                .WithOne().HasForeignKey(con => con.TenantsId);

            modelBuilder.Entity<Customer>()
              .HasOne<FormType>(c => c.FormType)
              .WithMany(f => f.Customers)
              .HasForeignKey(c => c.FormTypeId);

            modelBuilder.Entity<Customer>()
            .HasOne<Tenant>(c => c.Tenant)
            .WithMany(t=>t.Customers)
            .HasForeignKey(c => c.TenantsId);



        }

        public DbSet<demoPractice3.Models.RolePermission> RolePermission { get; set; }
      
    }
}
