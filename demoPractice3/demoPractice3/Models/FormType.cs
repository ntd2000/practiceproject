﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Models
{
    [Table("FormType")]
    public class FormType
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage ="This field is not empty")]
        [MaxLength(256)]
        public string Code { get; set; }

        [Required(ErrorMessage = "This field is not empty")]
        [MaxLength(1000)]
        public string Description { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }
    }
}
