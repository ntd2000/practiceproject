﻿using demoPractice3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Services
{
    public interface IRolePermissionService
    {
        public Task<List<RolePermission>> GetAll();
        public Task<RolePermission> GetById(Guid RoleId,Guid PermissionId);
        public Task<string> Add(RolePermission rolePermission);
        public Task<string> Update(Guid RoleId,Guid PermissionId, RolePermission rolePermission);
        public Task<string> Delete(Guid RoleId,Guid PermissionId);
    }
}
