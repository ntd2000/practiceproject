﻿using demoPractice3.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Services
{
    public class PermissionService : IPermissionService
    {
        private readonly MyDBContext _context;
        public PermissionService(MyDBContext context)
        {
            _context = context;
        }
        public async Task Add(Permission permission)
        {
            permission.Id = Guid.NewGuid();
            _context.Permissions.Add(permission);
            await _context.SaveChangesAsync();
        }

        public async Task<string> Delete(Guid id)
        {
            var permission = _context.Permissions.Find(id);
            if (permission == null)
            {
                return "null";
            }

            _context.Permissions.Remove(permission);
            await _context.SaveChangesAsync();
            return "deleted";
        }

        public async Task<List<Permission>> GetAll()
        {
            var data = await _context.Permissions.ToListAsync();
            return data;
        }

        public Permission GetById(Guid id)
        {

            var data =  _context.Permissions.Find(id);
            return data;
        }

        public async Task<List<Permission>> GetByRoleId(Guid id)
        {
            List<Permission> list = new List<Permission>();
            var linq = _context.RolePermission.Where(y => y.RoleId == id).Select(y => y.PermissionId).Distinct().ToList();

            if (linq != null)
            {
                foreach (Guid item in linq)
                {
                     var temp =_context.Permissions.Where(x => x.Id ==item ).Select(s => new Permission{ Id = s.Id, PermissionType = s.PermissionType }).ToList();
                        list.Add(temp[0]);
                }
            }

            return list;
            
        }

        public async Task<string> Update(Guid id, Permission permission)
        {
            _context.Entry(permission).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!(_context.Permissions.Any(e => e.Id == id)))
                {
                    return "null";
                }
                else
                {
                    throw;
                }
            }
            return "updated";
        }
    }
}
