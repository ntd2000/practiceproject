﻿using demoPractice3.Models;
using demoPractice3.Models.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Services
{
    public interface IUserService
    {
        public Task<List<User>> GetAll();
        public Task<User> GetById(Guid id);
        public Task Add(User user);
        public Task<string> Update(Guid id, User user);
        public Task<string> Delete(Guid id);

        public Task<User> GetUserByLogin(Login user);

        public Task<User> GetUserByMail(Mail mail);
        public List<Guid> GetPermissionByUser(User user);

        public Task<string> ChangePassword(Guid id,ChangPassword model);

    }
}
