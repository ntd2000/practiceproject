﻿using demoPractice3.Models;
using demoPractice3.Models.RequestModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Services
{
    public class RoleService : IRoleService
    {
        private readonly MyDBContext _context;
        
        public RoleService(MyDBContext context)
        {
            _context = context;
        }
        public async Task Add(RoleRequest role)
        {
            Role newRole = new Role
            {
                NameRole = role.NameRole
            };
            newRole.Id = Guid.NewGuid();
            _context.Roles.Add(newRole);
            if(role.PermissionId.Count>0)
            {
                foreach (var item in role.PermissionId)
                {
                    RolePermission temp = new RolePermission
                    {
                        RoleId = newRole.Id,
                        PermissionId = item
                    };
                    _context.RolePermission.Add(temp);
                }
            }
           

            
                await _context.SaveChangesAsync();
            
            
        }

        public async Task<string> Delete(Guid id)
        {
            var role =  _context.Roles.Find(id);

            if (role == null)
            {
                return "null";
            }
            try
            {
                _context.Roles.Remove(role);

                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return "foreignkey";
            }
            return "deleted";
        }

        public async Task<List<Role>> GetAll()
        {
            var data = await _context.Roles.ToListAsync();
            return data;
        }

        public async Task<Role> GetById(Guid id)
        {

            var data = await _context.Roles.FindAsync(id);
            return data;
        }

        public async Task<string> Update(Guid id, RoleRequest role)
        {
            //_context.Entry(role).State = EntityState.Modified;
            Role newRole = new Role
            {
                Id=id,
                NameRole=role.NameRole
            };
            _context.Roles.Update(newRole);
            var temp = _context.RolePermission.Where(x => x.RoleId == id);
            if(temp.Count()>0)
            {
                foreach (var item in temp)
                {
                    _context.RolePermission.Remove(item);
                }
            }
          
            if (role.PermissionId.Count > 0)
            {
                foreach (var item in role.PermissionId)
                {
                    RolePermission newItem = new RolePermission
                    {
                        RoleId = newRole.Id,
                        PermissionId = item
                    };
                    _context.RolePermission.Add(newItem);
                }
            }
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!(_context.Roles.Any(e => e.Id == id)))
                {
                    return "null";
                }
                else
                {
                    throw;
                }
            }
            return "updated";
        }
    }
}
