﻿using demoPractice3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Services
{
    public class RefreshTokenService : IRefreshTokenService
    {
        private readonly MyDBContext _context;
        public RefreshTokenService(MyDBContext context)
        {
            _context = context;
        }
        public void Add(RefreshToken model)
        {            
            _context.RefreshTokens.Add(model);
            _context.SaveChanges();
        }

        public RefreshToken Compare(TokenModel model)
        {
            var data=_context.RefreshTokens.FirstOrDefault(x => x.Token == model.RefreshToken);
            return data;
        }

        public void Edit(RefreshToken model)
        {
            _context.RefreshTokens.Update(model);
            _context.SaveChanges();
        }
    }
}
