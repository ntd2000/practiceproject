﻿using demoPractice3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Services
{
    public interface IFormTypeService
    {
        public Task<List<FormType>> GetAll();
        public Task<FormType> GetById(Guid id);
        public Task Add(FormType formType);
        public Task<string> Update(Guid id, FormType formType);
        public Task<string> Delete(Guid id);
    }
}
