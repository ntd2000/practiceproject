﻿using demoPractice3.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Services
{
    public class TenantService : ITenantService
    {
        private readonly MyDBContext _context;
        public TenantService(MyDBContext context)
        {
            _context = context;
        }
        public async Task Add(Tenant tenant)
        {
            tenant.Id = Guid.NewGuid();
            _context.Tenants.Add(tenant);
            await _context.SaveChangesAsync();
        }

        public async Task<string> Delete(Guid id)
        {
            var tenant = _context.Tenants.Find(id);
            if (tenant == null)
            {
                return "null";
            }
            try
            {
                _context.Tenants.Remove(tenant);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return "foreignkey";
            }
            return "deleted";
        }

        public async Task<List<Tenant>> GetAll()
        {
            var data = await _context.Tenants.ToListAsync();
            return data;
        }

        public async Task<Tenant> GetById(Guid id)
        {

            var data = await _context.Tenants.FindAsync(id);
            return data;
        }

        public async Task<string> Update(Guid id, Tenant tenant)
        {
            _context.Entry(tenant).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!(_context.Tenants.Any(e => e.Id == id)))
                {
                    return "null";
                }
                else
                {
                    throw;
                }
            }
            return "updated";
        }
    }
}
