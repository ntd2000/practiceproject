﻿using demoPractice3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Services
{
    public interface IPermissionService
    {
        public Task<List<Permission>> GetAll();
        public Permission GetById(Guid id);

        public Task<List<Permission>> GetByRoleId(Guid id);

        public Task Add(Permission permission);
        public Task<string> Update(Guid id, Permission permission);
        public Task<string> Delete(Guid id);

    }
}
