﻿using demoPractice3.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly MyDBContext _context;
        public CustomerService(MyDBContext context)
        {
            _context = context;
        }
        public async Task Add(Customer customer)
        {
            customer.Id = Guid.NewGuid();
            _context.Customers.Add(customer);
            await _context.SaveChangesAsync();          
        }

        public async Task<string> Delete(Guid id)
        {
            var customer = _context.Customers.Find(id);
            if (customer == null)
            {
                return "null";
            }

            _context.Customers.Remove(customer);
            await _context.SaveChangesAsync();
            return "Deleted";
        }

        public async Task<List<Customer>> GetAll()
        {
            var data = await _context.Customers.ToListAsync();
            return data;
        }

        public async Task<Customer> GetById(Guid id)
        {
            var data= await _context.Customers.FindAsync(id);
            return data;
        }

        public async Task<string> Update(Guid id, Customer customer)
        {
            _context.Entry(customer).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!(_context.Customers.Any(e => e.Id == id)))
                {
                    return "null";
                }
                else
                {
                    throw;
                }
            }
            return "updated";
        }
    }

}
