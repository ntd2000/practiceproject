﻿using demoPractice3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Services
{
    public interface ICustomerService
    {
        public Task<List<Customer>> GetAll();
        public Task<Customer> GetById(Guid id);
        public Task Add(Customer customer);
        public Task<string> Update(Guid id, Customer customer);
        public Task<string> Delete(Guid id);
    }
}
