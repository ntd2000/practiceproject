﻿using demoPractice3.Models;
using demoPractice3.Models.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Services
{
    public interface IRoleService
    {
        public Task<List<Role>> GetAll();
        public Task<Role> GetById(Guid id);
        public Task Add(RoleRequest role);
        public Task<string> Update(Guid id, RoleRequest role);
        public Task<string> Delete(Guid id);
    }
}
