﻿using demoPractice3.Models;
using demoPractice3.Models.RequestModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using demoPractice3.Security;

namespace demoPractice3.Services
{
    public class UserService : IUserService
    {
        private readonly MyDBContext _context;
        public UserService(MyDBContext context)
        {
            _context = context;
        }
        public async Task Add(User user)
        {
            user.Id = Guid.NewGuid();
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
        }

        public async Task<string> Delete(Guid id)
        {
            User user = _context.Users.Find(id);
            if (user == null)
            {
                return "null";
            }

            _context.Users.Remove(user);
            var data = _context.RefreshTokens.Where(x => x.UserId==user.Id);
            foreach(var item in data)
            {
                _context.RefreshTokens.Remove(item);
            }
            
              await _context.SaveChangesAsync();

         



            return "deleted";
        }

        public async Task<List<User>> GetAll()
        {
            var data = await _context.Users.ToListAsync();
            return data;
        }

        public async Task<User> GetById(Guid id)
        {

            var data = await _context.Users.FindAsync(id);
            return data;
        }

        public async Task<string> Update(Guid id, User user)
        {             
            _context.Entry(user).State = EntityState.Modified;   
            try
            {
                
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!(_context.Users.Any(e => e.Id == id)))
                {
                    return "null";
                }
                else
                {
                    throw;
                }
            }
            return "updated";
        }

        public async Task<User> GetUserByLogin(Login user)
        {

            var data =await _context.Users.SingleOrDefaultAsync(x=>x.Email==user.Email && x.Password==user.Password);
            return data;
            
        }

        public async Task<User> GetUserByMail(Mail mail)
        {

            var data = await _context.Users.SingleOrDefaultAsync(x => x.Email == mail.Email);
            return data;

        }

        public List<Guid> GetPermissionByUser(User user) 
        {
            List<Guid> list = new List<Guid>();
            var temp = _context.RolePermission.Where(x => x.RoleId == user.RoleId).Select(x=>x.PermissionId);
            if (temp.Count() > 0)
            {
                foreach (Guid item in temp)
                {
                   
                    list.Add(item);
                }
            }
            return list;

        }

        public async Task<string> ChangePassword(Guid id,ChangPassword model)
        {
            User user = _context.Users.FirstOrDefault(x => x.Id == model.Id);
            if(model.OldPassword!=SecurityHelper.Decrypt(user.Password))
            {
                return "Not correct old password";
            }
            user.Password = SecurityHelper.Encrypt(model.NewPassword);
            _context.Entry(user).State = EntityState.Modified;
            try
            {

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!(_context.Users.Any(e => e.Id == id)))
                {
                    return "null";
                }
                else
                {
                    throw;
                }
            }
            return "updated";
        }
    }
}
