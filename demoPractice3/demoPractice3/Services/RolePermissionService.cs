﻿using demoPractice3.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Services
{
    public class RolePermissionService : IRolePermissionService
    {
        private readonly MyDBContext _context;
        public RolePermissionService(MyDBContext context)
        {
            _context = context;
        }
        public async Task<string> Add(RolePermission rolePermission)
        {

            _context.RolePermission.Add(rolePermission);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if ((_context.RolePermission.Any(e => e.RoleId == rolePermission.RoleId && e.PermissionId == rolePermission.PermissionId)))
                {
                    return "conflict";
                }
                else
                {
                    throw;
                }
            }
            return "add";
        }

        public async Task<string> Delete(Guid RoleId,Guid PermissionId)
        {
            var data = _context.RolePermission.FirstOrDefault(x=> x.RoleId== RoleId&& x.PermissionId==PermissionId);
            if (data == null)
            {
                return "null";
            }

            _context.RolePermission.Remove(data);
            await _context.SaveChangesAsync();
            return "deleted";
        }

        public async Task<List<RolePermission>> GetAll()
        {
            var data = await _context.RolePermission.ToListAsync();
            return data;
        }

        public async Task<RolePermission> GetById(Guid RoleId,Guid PermissionId)
        {

            var data = await _context.RolePermission.FirstOrDefaultAsync(x=> x.RoleId==RoleId && x.PermissionId==PermissionId);
            return data;
        }

        public async Task<string> Update(Guid RoleId,Guid PermissionId , RolePermission rolePermission)
        {
            _context.Entry(rolePermission).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!(_context.RolePermission.Any(e => e.RoleId == RoleId && e.PermissionId==PermissionId)))
                {
                    return "null";
                }
                else
                {
                    throw;
                }
            }
            return "updated";
        }
    }
}
