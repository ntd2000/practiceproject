﻿using demoPractice3.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Services
{
    public class FormTypeService : IFormTypeService
    {
        private readonly MyDBContext _context;
        public FormTypeService(MyDBContext context)
        {
            _context = context;
        }
        public async Task Add(FormType formType)
        {
            formType.Id = Guid.NewGuid();
            _context.FormTypes.Add(formType);
            await _context.SaveChangesAsync();
        }

        public async Task<string> Delete(Guid id)
        {
            var formType = _context.FormTypes.Find(id);
            if (formType == null)
            {
                return "null";
            }
            try
            {
                _context.FormTypes.Remove(formType);
                await _context.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                return "foreignkey";
            }
          
            return "deleted";


        }

        public async Task<List<FormType>> GetAll()
        {
            var data = await _context.FormTypes.ToListAsync();
            return data;
        }

        public async Task<FormType> GetById(Guid id)
        {

            var data = await _context.FormTypes.FindAsync(id);
            return data;
        }

        public async Task<string> Update(Guid id, FormType formType)
        {
            _context.Entry(formType).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!(_context.FormTypes.Any(e => e.Id == id)))
                {
                    return "null";
                }
                else
                {
                    throw;
                }
            }
            return "updated";
        }
    }
}
