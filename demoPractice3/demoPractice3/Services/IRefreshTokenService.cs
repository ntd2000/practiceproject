﻿using demoPractice3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Services
{
    public interface IRefreshTokenService
    {
        public void Add(RefreshToken model);
        public void Edit(RefreshToken model);
        public RefreshToken Compare(TokenModel model);
    }
}
