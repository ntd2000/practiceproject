﻿using demoPractice3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.Services
{
    public interface ITenantService
    {
        public Task<List<Tenant>> GetAll();
        public Task<Tenant> GetById(Guid id);
        public Task Add(Tenant tenant);
        public Task<string> Update(Guid id, Tenant tenant);
        public Task<string> Delete(Guid id);
    }
}
