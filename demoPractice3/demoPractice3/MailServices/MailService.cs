﻿using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace demoPractice3.MailServices
{
    public class MailService : IMailService
    {
        MailSettings _mailSettings { get; set; }
        public MailService(IOptions<MailSettings> mailSettings)
        {
            _mailSettings = mailSettings.Value;
        }
   

        public async Task<string> SendEmail(MailContent mailContent)
        {
            var email = new MimeMessage();
            email.Sender = new MailboxAddress(_mailSettings.DisplayName, _mailSettings.Mail);
            email.From.Add(new MailboxAddress(_mailSettings.DisplayName, _mailSettings.Mail));
            email.To.Add(MailboxAddress.Parse(mailContent.To));
            email.Subject = mailContent.Subject;

            var builder = new BodyBuilder();
            builder.HtmlBody = mailContent.Body;
            email.Body = builder.ToMessageBody();

            // dùng SmtpClient của MailKit
            using var smtp = new MailKit.Net.Smtp.SmtpClient();

            try
            {
                smtp.Connect(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTls);
                smtp.Authenticate(_mailSettings.Mail, _mailSettings.Password);
                await smtp.SendAsync(email);
                return "send mail success";
            }
            catch (Exception ex)
            {
                return "send mail fail";
            }

            smtp.Disconnect(true);
        }

        public async Task<string> SendEmailAsync(string email, string subject, string htmlMessage)
        {
            var data=await SendEmail(new MailContent()
            {
                To = email,
                Subject = subject,
                Body = htmlMessage
            });
            return data;
        }
    }
}
