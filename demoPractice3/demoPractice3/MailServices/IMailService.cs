﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demoPractice3.MailServices
{
    public interface IMailService
    {
        public Task<string> SendEmail(MailContent mailContent);
        public Task<string> SendEmailAsync(string email, string subject, string htmlMessage);
    }
}
